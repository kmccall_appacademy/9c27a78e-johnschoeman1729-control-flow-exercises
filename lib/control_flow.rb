# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  str.each_char do |ch|
    str.delete!(ch) if ('a'..'z').include?(ch)
  end
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  if str.length.even?
    return str[str.length/2-1..str.length/2]
  else
    return str[str.length/2]
  end
end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
  count = 0
  str.each_char { |ch| count += 1 if VOWELS.include?(ch) }
  count
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  return 1 if num == 1
  num * factorial(num - 1)
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  res = ""
  (0...arr.length-1).each do |i|
    res << arr[i] << separator
  end
  res << arr[-1].to_s
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  res = ""
  str.each_char.with_index do |ch, i|
    if i.even?
      res << ch.downcase
    else
      res << ch.upcase
    end
  end
  res
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  res = str.split.map { |word| word.length > 4 ? word.reverse : word }
  res.join(" ")
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  (1..n).to_a.map do |num|
    if num % 15 == 0
      "fizzbuzz"
    elsif num % 5 == 0
      "buzz"
    elsif num % 3 == 0
      "fizz"
    else
      num
    end
  end
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  res = []
  arr.length.downto(0) do |i|
    res << arr[i]
  end
  res.compact
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  return false if num == 1
  return true if num < 4
  (2...num).each do |n|
    return false if num % n == 0
  end
  true
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  res = []
  (1..num).each do |n|
    res << n if num % n == 0
  end
  res
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  factor_arr = factors(num)
  factor_arr.select { |num| prime?(num) }
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  prime_factors(num).length
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  evens = arr.select { |num| num.even? }
  odds = arr.select { |num| num.odd? }
  if evens.length == 1
    return evens[0]
  else
    return odds[0]
  end
end
